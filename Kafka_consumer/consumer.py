import kafka
import importlib
from ProtoC import switch_pb2
from Influx_inserter.encoder import Encoder
from Influx_inserter.insert import Insert

from Classes.setting import Setting


class Consumer:
    def __init__(self):
        setting = Setting().get_section_settings('SectionKafka')
        self.topic = setting["topic"]
        host = setting["host"]
        self.influx = Insert()
        try:
            self.metric = importlib.import_module("ProtoC.", setting["ProtoBuf"])
        except (ImportError, KeyError):
            self.metric = importlib.import_module("ProtoC", "switch_pb2")

        self.consumer = kafka.KafkaConsumer(bootstrap_servers=host)
        self.info = switch_pb2.Device()

    def __del__(self):
        pass

    def consume(self):
        self.consumer.subscribe(self.topic)
        for message in self.consumer:
            try:
                self.info.ParseFromString(message[6])
    #         todo in thread
                self.to_influxdb()
            except IndexError, e:
                print e
                pass

    def to_influxdb(self):
        line = [Encoder().device(self.info)]
        for interface in self.info.interfaces:
            line.append(Encoder().interfaces(self.info.name, interface))
        self.influx.insert(line)
