from Classes.setting import Setting

import influxdb
import logging


class Insert:
    def __init__(self):
        setting = Setting().get_section_settings('SectionInfluxDB')
        host = setting['host']
        port = setting['port']
        username = setting['username']
        password = setting['password']
        self.database = setting['database']
        self.client = influxdb.InfluxDBClient(host=host)
        self.client = influxdb.InfluxDBClient(port=port)
        self.client = influxdb.InfluxDBClient(username=username)
        self.client = influxdb.InfluxDBClient(password=password)
        self.client = influxdb.InfluxDBClient(database=self.database)
        self.create_db(self.database)

    def __del(self):
        del self.client

    def create_db(self, database):
        """ checks database existence and creates one if it does not exist
        :param database: database name to create
        :type database: str
        :return: None
        """
        if database not in self.client.get_list_database():
            logging.info("created database: %s" % database)
            self.client.create_database(database)
        else:
            pass

    def drop_db(self, database):
        """ checks database existence and deletes it
        :param database: database name to delete
        :type database: str
        :return: None
        """
        if database not in self.client.get_list_database():
            pass
        else:
            logging.info("droped database: %s" % database)
            self.client.drop_database(database)

    def select(self, fields, measurement):
        """
        :param fields:
        :param measurement:
        :return:
        """
        # TODO implement search based on list
        # self.client.query("select ")
        pass

    def insert(self, data):
        """ function to insert formatted line data in the influxdb server
        :param data: Points to be inserted to the influxdb
        :type data: list or str
        :return result of the influxdb
        :rtype boolean
        """
        if type(data) == 'list':
            return self.client.write_points(points=data, time_precision="ms", database=self.database,
                                            retention_policy='default', protocol='line')
        else:
            return self.client.write(data, {'db': self.database, 'precision': "ms"}, protocol='line')
