import time
import logging


class Encoder:

    def __init__(self):
        pass

    def __del(self):
        pass

    @staticmethod
    def device(device):
        """
        :param device: list of protobuf decoded message
        :return: encoded string for influxDB
        :rtype str
        :except ValueError
        """
        try:
            return ('Monitoring,Name=%s Serial="%s",Brand="%s",Model="%s",softwareVersion="%s",envState=%s,CPUload=%s,'
                    'MemoryUsage=%s,Error="%s" %s' %
                    (device.name.rstrip(), device.serial, device.make, device.model,
                     device.softwareVersion.replace(' ', '\ '), device.envState, device.cpuLoad,
                     device.memoryUsage, device.envError, str(int(round(time.time() * 1000)))))
        except ValueError, e:
            logging.error(e)
            raise ValueError

    @staticmethod
    def interfaces(name, interface):
        try:
            interface.ifAlias = "alias"
            return ('Interfaces,FQDN=%s,Interface=%s,Alias=%s Index=%s,AdminState=%s,OperatorState=%s,InBytes=%s,'
                    'OutBytes=%s,InErrors=%s,OutErrors=%s,InDiscards=%s,OutDiscards=%s,MTU=%s,Buffer=%s %s'
                    % (name.rstrip(), interface.ifDescr, interface.ifAlias, interface.ifIndex,
                       interface.ifAdminStatus, interface.ifOperStatus, interface.ifInOctets, interface.ifOutOctets,
                       interface.ifInErrors, interface.ifOutErrors, interface.ifInDiscards, interface.ifOutDiscards,
                       interface.ifMtu, interface.ifInBuffer, str(int(round(time.time() * 1000)))))
        except ValueError, e:
            logging.error(e)
            raise ValueError

