# Network-Telemetry 
## About
This repository contains the code used for the network telemetry monitoring solution.
Documentation will be updated once development has started.

## Repository Rules
This repository uses the best practices supplied by git flow, this standard protects the master and development
branches against pushing and direct merging.

Information about git flow is found here: http://nvie.com/posts/a-successful-git-branching-model/ 

To submit code please create a feature branch from the develop or master branch.
Once the feature is finished you can submit it by submitting a merge request on the web page.

### How to create a feature
`git checkout -b feature/Myfeature develop`

or 

`git checkout -b feature/Myfeature master`

### How to submit a merge request
Use this guide found on the gitlab page.

https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html 


