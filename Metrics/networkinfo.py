from ProtoC import switch_pb2
import subprocess
import json
import logging
import os


class NetworkInfo:

    def __init__(self):
        """Init function, creates the class variables and calls __get_eeprom"""
        self.infCount = 0
        self.info = None
        self.interfaceNames = None
        self.switch = None
        self.serial = None
        self.make = None
        self.model = None
        self.__get_eeprom()

    def __del__(self):
        """Delete function deletes all files as supposed, this prevents potential memory leaks"""
        del self.infCount
        del self.info
        del self.interfaceNames
        del self.switch
        del self.serial
        del self.make
        del self.model

    def populate(self):
        """populate function creates the device and adds all network interfaces to the switch variable
        :return self.switch
        """
        self.switch = self.create_device()
        i = 0
        self.get_interfaces()
        while i <= self.infCount:
            self.add_interface(i)
            i += 1

    def get_interfaces(self):
        """populates self.info, self.interfaceNames and self.infCount
        :return: all interfaces in the device
        """
        self.interfaceNames = self.list_dir("/sys/class/net/")
        self.infCount = len(self.interfaceNames) - 1

    def add_interface(self, i):
        """adds interface to the switch object
        :param i: representative number in the info list
        :type: int
        """
        name = self.interfaceNames[i]
        interface = self.switch.interfaces.add()
        interface.ifIndex = int(self.read_file('/sys/class/net/%s/ifindex' % name))
        interface.ifDescr = name
        interface.ifAlias = self.read_file('/sys/class/net/%s/ifalias' % name)

        interface.ifAdminStatus = self.get_adminstate()
        interface.ifOperStatus = self.operstate(name)
        interface.ifInOctets = int(self.read_file("/sys/class/net/%s/statistics/rx_bytes" % name))
        interface.ifOutOctets = int(self.read_file("/sys/class/net/%s/statistics/tx_bytes" % name))
        interface.ifInErrors = int(self.read_file("/sys/class/net/%s/statistics/rx_errors" % name))
        interface.ifOutErrors = int(self.read_file("/sys/class/net/%s/statistics/tx_errors" % name))
        interface.ifInDiscards = int(self.read_file("/sys/class/net/%s/statistics/rx_dropped" % name))
        interface.ifOutDiscards = int(self.read_file("/sys/class/net/%s/statistics/tx_dropped" % name))

        interface.ifMtu = int(self.read_file("/sys/class/net/%s/mtu" % name))
        interface.ifInBuffer = 0  # TODO retrieve Buffer

    def create_device(self):
        """creates the device object based on switch_pb2.py
        :return: switch object
        """
        switch = switch_pb2.Device()
        switch.name = self.read_file("/proc/sys/kernel/hostname")
        switch.serial = self.serial
        switch.make = self.make
        switch.model = self.model
        switch.softwareVersion = self.get_release()
        switch.cpuLoad = self.get_cpu()
        switch.memoryUsage = self.get_mem()
        switch.envState = 1  # todo read envstate
        switch.envError = "Failing Fan"  # todo read error
        return switch

    def __get_eeprom(self):
        """__get_eeprom creates populates self.serial, self.make and self.model with information cached from
         eeprom using syscall decode-syseeprom -j
        :return: self.serial
        :return self.make
        :return self.model
        :except: ValueError
        :except: OSError
        """
        eeprom = None
        try:
            eeprom = json.loads(subprocess.check_output(['/usr/cumulus/bin/decode-syseeprom', '-j']))
        except OSError, e:
            logging.error("Cannot Call decode-syseeprom, Error: %s" % e)
            exit(1)

        try:
            self.serial = eeprom['tlv']['Serial Number']['value']
            self.make = eeprom['tlv']['Vendor Name']['value']
            self.model = eeprom['tlv']['Product Name']['value']
        except ValueError as v:
            logging.error(v)
        except TypeError as t:
            logging.error(t)

    @staticmethod
    def get_release():
        """get_release function reads /etc/lsb-release file and parses the DISTRIB_DESCRIPTION key
        :return release
        :except IndexError
        :except OSError
        """
        try:
            f = open("/etc/lsb-release")
            identifier = "DISTRIB_DESCRIPTION="
            output = f.read()
            f.close()
            index = output.index(identifier) + len(identifier) + 1
            return output[index:-2]
        except (IndexError, OSError) as e:
            logging.error(e)

    @staticmethod
    def read_file(path):
        """reads file entire file
        :param: path: path to the file to read
        :type: string
        :return: string of file contents
        """
        try:
            f = open(path)
            return f.read()
        except OSError as e:
            logging.error("Error reading file: %s , Error: %s" % path, e)

    def operstate(self, interface):
        """operstate reads the string of interface and converts this to the appropriate status number
        :param interface:
        :return: operstate_nr
        """
        state = self.read_file('/sys/class/net/%s/operstate' % interface)
        if state == "unknown":
            return 0
        if state == "notpresent":
            return 1
        if state == "down":
            return 2
        if state == "lowerlayerdown":
            return 3
        if state == "testing":
            return 4
        if state == "dormant":
            return 5
        if state == "up":
            return 6
        else:
            return 0

    @staticmethod
    def get_mem():
        # TODO implement memory monitoring
        return 0

    @staticmethod
    def get_cpu():
        # TODO implement cpu monitoring
        return 0

    @staticmethod
    def get_adminstate():
        # TODO implement adminstate
        return 0

    def serialize(self):
        """serializes the string using google protocol buffers to optimize memory usage
        :return: encoded gpb string
        """
        return self.switch.SerializeToString()

    @staticmethod
    def list_dir(path):
        """lists directories without lo or bonding_masters
        :param path: path to list
        :return list of directories
        """
        dirs = []
        for directory in os.listdir(path):
            if directory == "lo" or directory == "bonding_masters":
                pass
            else:
                dirs.append(directory)

        return dirs

