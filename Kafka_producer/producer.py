import kafka
import importlib
from Classes.setting import Setting


class Producer:
    def __init__(self):
        setting = Setting().get_section_settings('SectionKafka')
        host = setting["host"]
        self.topic = setting["topic"]

        try:
            # todo check implementation
            self.metric = importlib.import_module(str("Metrics." + setting["metric"]), setting["class"])
        except (ImportError, KeyError):
            self.metric = importlib.import_module("Metrics.networkinfo").NetworkInfo()
        # self.metric = NetworkInfo()

        self.kafka = kafka.KafkaProducer(bootstrap_servers=host)
        self.info = None

    def __del__(self):
        pass

    def sender(self):
        self.kafka.send(self.topic, self.metric.serialize())

    def get_info(self):
        self.metric.populate()

    def produce(self):
        self.get_info()
        self.sender()



