import ConfigParser
import os
import logging


class Setting:
    """
        Settings class represents settings found in the settings.ini file.

        Requires:
            settings.ini to be present
    """

    __SETTINGS_FILE = os.path.dirname(__file__) + '/../settings.ini'
    __SYSTEM_SECTION = 'SectionSystem'
    __PARSER = None

    def __init__(self):
        self.__PARSER = ConfigParser.ConfigParser()
        self.__PARSER.read(self.__SETTINGS_FILE)

    def get_sections(self):
        return self.__PARSER.sections()

    def get_section_settings(self, section):
        settings = {}
        options = self.__PARSER.options(section)
        for option in options:
            try:
                settings[option] = self.__PARSER.get(section, option)
            except ValueError as e:
                logging.error(e)
                settings[option] = None
            except KeyError as e:
                logging.error(e)
                settings[option] = None

        return settings

    def set_section_setting(self, option, value):
        writefile = open(self.__SETTINGS_FILE, 'w')
        self.__PARSER.set(self.__SYSTEM_SECTION, option, value)
        self.__PARSER.write(writefile)
