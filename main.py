#!/usr/bin/python
from Kafka_producer.producer import Producer
from Classes.setting import Setting
from Kafka_consumer.consumer import Consumer
import daemon
import logging
from time import sleep

setting = Setting().get_section_settings('Global')

try:
    logging.basicConfig(filename=setting["logfile"], format='%(levelname)s:%(message)s', level=logging.INFO)
except ValueError:
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

with daemon.DaemonContext():
    if setting["mode"].upper() == "CONSUMER":
        consumer = Consumer()
        while True:
            consumer.consume()
    elif setting["mode"].upper() == "PRODUCER":
        producer = Producer()
        while True:
            producer.produce()
            sleep(float(setting['timeout']))
    else:
        exit("No Mode selected")
